import numpy as np
from matplotlib import pyplot as plt

data_array = np.zeros((10, 4))

with open('pho4_reg_r1_s1.txt', 'r') as data_file:
    for i, lines in enumerate(data_file):
        data_array[i, :] = np.array(lines.split())

    print(data_array)

data_array_flat = list(data_array.flatten())
print(data_array_flat)

first_vect = data_array_flat[0:4]
first_two_vec = data_array_flat[0:8]
# first

plt.bar([i for i in range(8)], first_two_vec, 0.2)
plt.xticks([i for i in range(8)], [(j // 4 + 1) for j in range(8)])

for i in range(len(data_array_flat)):
    print((i // 4) + 1)

# ### GROUPING THE DATA!

print([i for i in range(1, len(data_array_flat), 4)])

np.arange(1, 40)
N = 10

ind = np.arange(N)  # x-axis
width = 0.15
#
# a_dim = [data_array_flat[0], data_array_flat[4]] #y-axis for 0 dim
# t_dim = [data_array_flat[1], data_array_flat[5]] # ...
# g_dim = [data_array_flat[2], data_array_flat[6]]
# c_dim = [data_array_flat[3], data_array_flat[7]]


# some_dim = [data_array_flat[i], i for i in range(0, 160, 4)]
a_dim = [data_array_flat[i] for i in range(0, 40, 4)]  # y-axis for 0 dim
c_dim = [data_array_flat[i] for i in range(1, 40, 4)]
g_dim = [data_array_flat[i] for i in range(2, 40, 4)]
t_dim = [data_array_flat[i] for i in range(3, 40, 4)]

fig, ax = plt.subplots()

p1 = ax.bar(ind, a_dim, width, color='g')
p2 = ax.bar(ind + width, t_dim, width, color='r')
p3 = ax.bar(ind + 2 * width, g_dim, width, color='y')
p4 = ax.bar(ind + 3 * width, c_dim, width, color='b')

ax.set_xticks(ind)
# ax.set_xticklabels(('1', '2'))
ax.set_xticklabels(np.arange(1, 11))

ax.legend((p1[0], p2[0], p3[0], p4[0]), ('A', 'T', 'G', 'C'))

#plt.show()
plt.savefig('pho4_reg_r1_s1_colors.png')




