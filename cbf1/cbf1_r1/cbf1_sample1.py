from numpy import *
import numpy as np
import matplotlib.pyplot as plt
import sr
import sys
import time
start_time = time.time()
N = 10000 #after randomly subsampling 10,000 rows from the larger dataset

dm = 4  # Dimension
sites = 10  # Number of sites

# ------- Getting trait values from file 'trait.dat'-----------
F = genfromtxt('cbf1_rep1_sample1_ddg.txt')  # file containing trait values that will be mapped to sequence
Fest = genfromtxt('cbf1_rep1_sample1_ddg.txt') # vectors that must be the same size as F
Fon1 = genfromtxt('cbf1_rep1_sample1_ddg.txt')
Fon2i1 = genfromtxt('cbf1_rep1_sample1_ddg.txt')
Fon12 = genfromtxt('cbf1_rep1_sample1_ddg.txt')

for i in range(N):
   Fest[i] = 0
   Fon1[i] = 0
   Fon2i1[i] = 0
   Fon12[i] = 0

#-------------
#file = open('cbf1_rep1_seqs.txt')
file = open('cbf1_rep1_sample1.txt')
seq = file.readlines()

# ----Initializing various terms that we will use.--------------

# 3 sites, each a dm dim vector, in N individuals
# NOTE: For application to Amino Acid sequences, increase
# the size of the arrays accordingly.
phi = array([[[0.0 for k in range(dm)] for i in range(N)] for j in range(sites)])  # general enough for all sites
mean = array([[0.0 for z in range(dm)] for i in range(sites)])
var = array([[0.0 for z in range(dm)] for i in range(sites)])
phi2 = array([[[[[0.0 for k in range(dm)] for i in range(dm)] for j in range(N)] for l in range(sites)] for m in range(sites)])
phi2m = array([[[[0.0 for k in range(dm)] for i in range(dm)] for l in range(sites)] for m in range(sites)])

phi3 = array([[[[0.0 for k in range(dm)] for i in range(dm)] for j in range(dm)] for l in range(N)])
phi3m = array([[[0.0 for k in range(dm)] for i in range(dm)] for j in range(dm)])

P = array([[[0.0 for z in range(dm)] for j in range(N)] for i in range(sites)])
#CM = array([[[0.0 for k in range(dm)] for i in range(sites)] for m in range(0, M)])
cov = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])

# # ------------Converting letters to vectors---------------
#
count = array([0.0 for j in range(sites)])
# #phi[individual][site][state]. phi[i][j] = vector for site j in individual i.
for i in range(0, N):  # individual
    for j in range(sites):
        if seq[i][j] == 'A':
            phi[j][i][0] = 1.0
            count[j] += 1
        if seq[i][j] == 'a':
            phi[j][i][0] = 1.0
            count[j] += 1
        if seq[i][j] == 'T':
            phi[j][i][1] = 1.0
            count[j] += 1
        if seq[i][j] == 't':
            phi[j][i][1] = 1.0
            count[j] += 1
        if seq[i][j] == 'G':
            phi[j][i][2] = 1.0
            count[j] += 1
        if seq[i][j] == 'g':
            phi[j][i][2] = 1.0
            count[j] += 1
        if seq[i][j] == 'C':
            phi[j][i][3] = 1.0
            count[j] += 1
        if seq[i][j] == 'c':
            phi[j][i][3] = 1.0
            count[j] += 1
        if phi[j][i][0] == 0.0:
            if phi[j][i][1] == 0.0:
                if phi[j][i][2] == 0.0:
                    if phi[j][i][3] == 0.0:
                        phi[j][i][0] = -1

# keep in alpha order

# ---------------------------------First order terms --------------------------------
np.set_printoptions(threshold=sys.maxsize)

print('before calculating means')
'''
This is where we will compute the means and check to see if the pandas structure will work.
'''
# calculate mean vectors #stays same
for i in range(0, N):
    for j in range(sites):
        mean[j] += phi[j][i] / count[j]  # divide by the count of how many nucleotides are present there, count[j]
#
#
print(mean)
print('saving means to: mean_s1.npy')
np.save("mean_s1.npy", mean)

# Subtracting the mean from each vector #stays the same
# P[site][indiv][nucleotide]
# for k in range(sites): # site
#     for i in range(0,N): # indiv
#         P[k][i] = phi[k][i] - mean[k]

# trying to stay consistent with calculating mean vectors code above
'''
This is where we subtract out the means.
'''
for j in range(sites):  # site
    for i in range(0, N):  # indiv
        P[j][i] = phi[j][i] - mean[j]

print(len(P))
print('P = phi values - mean')

print('saving P to: P_s1.npy')
np.save("P_s1.npy", P)

# #var[site][nucleotide]
for k in range(sites):
    for i in range(0, dm):  # nucleotide
        for j in range(0, N):  # individual
            var[k][i] += ((P[k][j][i]) ** 2)/N
#
# #np.set_printoptions(threshold = np.nan)
print('saving var to: var_s1.npy')
np.save("var_s1.npy", var)

# # # Covariances between nucleotides at sites i and j
# # # this is a matrix
# # # the cov matrix for the two sites is just the mean, across all individuals, of the outer product of P1 and P2
# # #P2 is site 2 with means subtracted out
# #
print("working on covs")
for j in range(sites):
    for k in range(sites):
        for i in range(N):
            cov[j][k] += sr.outer_general(P[j][i],P[k][i])/N

print('saving covariance matrix to: cov2_s1.npy')
np.save("cov2_s1.npy", cov) #the first cov was done with the seqs not filtered according to -inf ddg values

Pa = array([[[0.0 for z in range(dm)] for j in range(N)] for i in range(sites)])
reg11 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])

print("working on reg11")
# # Regression of site k on site l
# # reg11[2][1] is a matrix (hence two more indices) containing the regressions of
# # each element of the site 2 vector on each element of the site 1 vector
for k in range(sites):
    for l in range(sites):
        for i in range(0,dm):
            for j in range(0,dm):
                if var[l][j] > 0.0000000000001:
                    reg11[k][l][i][j] = cov[k][l][i][j]/var[l][j]
                else:
                    reg11[k][l][i][j] = 0

r1on2 = reg11[0][1]
r2on1 = reg11[1][0]
print('saving reg11 to: reg11_s1.npy')
np.save("reg11_s1.npy", reg11)

# # # print(type(Pa))
# # # print(Pa.shape)
# # # print(len(Pa.shape))
# # #
# # # #works fine until here (59 secs)
# # #113.5 when asked to print to a file
# # #
# # # # # First order terms with zeros except for the value that is
# # # # # present (i.e. orthogonalized within each vector)
print("working on PakiDim")
for k in range(sites): # site
    for i in range(0,N):  # indiv
        Pa[k][i] = sr.inner_general(sr.outer_general(phi[k][i], phi[k][i]), P[k][i])
        PakiDim = np.array(Pa[k][i])
        #srOuter11Dim = np.array(sr.outer_general(phi[k][i],phi[k][i]), P[k][i])
        #srinnerGenDim = np.array(sr.outer_general(phi[k][i],phi[k][i]), P[k][i])

# # #print("PakiDim =" + str(PakiDim.ndim))
print('saving PakiDim to: PakiDim_s1.npy')
np.save("PakiDim_s1.npy", PakiDim)



# # # print("srOuter11Dim =" + str(srOuter11Dim.ndim))
# # # print("srinnerGenDim =" + str(srinnerGenDim.ndim))

# # # # # Site j orthogonalized wrt site k
# # # # P1i1[j][k][i] =  first order phi of site j independent of site k for individual i
print("working on P1i1")
P1i1 = array([[[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)] for k in range(sites)])

for i in range(0,N): # Individuals
    for j in range(sites):
        for k in range(sites):
            if k != j:
                P1i1[j][k][i] = P[j][i] - sr.inner_general(reg11[j][k],Pa[k][i])

P2i1 = P1i1[1][0]

print('saving P1i1 to: P1i1_s1.npy')
np.save("P1i1_s1.npy", P1i1)


# # #with the printout file, 743.7 seconds
print("working on varP1i1")
# # # # Variance in P2i1
varP1i1 = array([[[0.0 for z in range(dm)] for i in range(sites)] for j in range(sites)])

for i in range(0,N): #individuals
    for j in range(sites):
        for k in range(sites):
            if k != j:
                varP1i1[j][k] += (P1i1[j][k][i]**2)/N

print('saving varP1i1 to: varP1i1_s1.npy')
np.save("varP1i1_s1.npy", varP1i1)

print("working on cov11i1")
# # # # cov11i1[j][k][l] = cov between site j and (site k independent of l)
cov11i1 = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)])

for j in range(sites):
    for k in range(sites):
        for l in range(sites):
            for i in range(N):
                cov11i1[j][k][l] += sr.outer_general(P[j][i],P1i1[k][l][i])/N

print('saving cov11i1 to: cov11i1_s1.npy')
np.save("cov11i1_s1.npy", cov11i1)

print("working on reg11i1")
# # # # # regression of site j on (site k independent of l)
reg11i1 = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)])

for k in range(sites):
    for l in range(sites):
        for m in range(sites):
            for i in range(0,dm):
                for j in range(0,dm):
                    if varP1i1[l][m][j] > 0.0000000000001:
                        reg11i1[k][l][m][i][j] = cov11i1[k][l][m][i][j]/varP1i1[l][m][j]
                    else:
                        reg11i1[k][l][m][i][j] = 0
print('saving reg11i1 to: reg11i1_s1.npy')
np.save("reg11i1_s1.npy", reg11i1)

# #dump cov11i1 = None
cov11i1 = None
#
# # # #ran but took 5903.9 secs
# # #with printoutput, took 38695.98 secs (but this may also be due to computer dying or sleep at night
print("working on Pa1i1")
# # # # # same as P1i1, except with all elements = 0 except the one present.
Pa1i1 = array([[[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)] for k in range(sites)])

for i in range(0,N):  # indiv
    for j in range(sites):
        for k in range(sites):
            if k != j:
                Pa1i1[j][k][i] = sr.inner_general(sr.outer_general(phi[j][i],phi[j][i]),P1i1[j][k][i])

print('saving Pa1i1 to: Pa1i1_s1.npy')
np.save("Pa1i1_s1.npy", Pa1i1)

print("working on P1D")
# # # # # P1D[j][i] = first order poly of site j independent of all other sites, for individual i
P1D = array([[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)])
#
for i in range(0,N):  # indiv
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    if l != k & l != j:
                        P1D[j][i] = P[j][i] - sr.inner_general(reg11i1[j][k][l],Pa1i1[k][l][i]) - sr.inner_general(reg11[j][l],Pa[l][i])

print('saving P1D to: P1D_s1.npy')
np.save("P1D_s1.npy", P1D)

reg11i1 = None
#
# #
# # # # ran but took 22440.35 secs (374 mins, over 6 hours)
# # #with print output, took 11521.18
# # # #  P1D with all elements = 0 except the one present
print("working on varP1D")
# # # #variance in P1D
varP1D = array([[0.0 for z in range(dm)] for i in range(sites)])

for k in range(sites):
    for i in range(0,dm):  # nucleotide
        for j in range(0,N):   # individual
            varP1D[k][i] += ((P1D[k][j][i])**2)/N

print('saving varP1D to: varP1D_s1.npy')
np.save("varP1D_s1.npy", varP1D)

Pa2i1 = Pa1i1[1][0]
varP2i1 = varP1i1[1][0]

# # # #-------------------------------------------------------------
# # # # ------------------------Second Order Terms -----------------
# # # #-------------------------------------------------------------

# # # Second order phenotypes.
for k in range(N):
    for i in range(sites):
        for j in range(sites):
            if j != i:
                phi2[i][j][k] = sr.outer(phi[i][k],phi[j][k])
                phi2m[i][j] += phi2[i][j][k]/N

phi12 = phi2[0][1]
phi12m = phi2m[0][1]
#
# with open('phi2m.txt','w') as f:
#     for i in range(len(phi2m)):
#         f. write('%s\n' %phi2m)
# # #
# # # # Q12 contains the 2'nd order phenotypes with the means subtracted out.
Q2 = array([[[[[0.0 for k in range(dm)] for i in range(dm)] for j in range(N)] for l in range(sites)] for m in range(sites)])
for i in range(N):  #indiv
    for j in range(sites):
        for k in range(sites):
            if k != j:
                Q2[j][k][i] = phi2[j][k][i] - phi2m[j][k]
    #Q12[i] = phi12[i] - phi12m

Q12 = Q2[0][1]
#
# with open('Q12.txt','w') as f:
#     for i in range(len(Q12)):
#         f. write('%s\n' %Q12)
# #
# # #with print output, took 11036.9 secs (approx 3.1 hours)
# # # # Covariance between elements of the 2'nd order phenotype matrix and
# # # # the 1'st order phenotype.

cov2w1 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    cov2w1[j][k][l] += sr.outer_general(Q2[j][k][i],P[l][i])/N
#
# cov12w1 = cov2w1[0][1][0]
# with open('cov2w1.txt','w') as f:
#     for i in range(len(cov2w1)):
#         f. write('%s\n' %cov2w1)
# #
# #
# # #
# # # # Covariance of second order phenotype matrices with first order phenotypes.
cov2w1a = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
cov2w1b = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
cov2w1c = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                cov2w1a[j][k] += sr.outer_general(Q2[j][k][i],P[0][i])/N
                cov2w1b[j][k] += sr.outer_general(Q2[j][k][i],P1i1[1][0][i])/N
                cov2w1c[j][k] += sr.outer_general(Q2[j][k][i],P1D[2][i])/N
# # #print(cov2w1a[0][1]-cov2w1test[0][0][1])
# with open('cov2w1a.txt','w') as f:
#     for i in range(len(cov2w1a)):
#         f. write('%s\n' %cov2w1a)
#
# with open('cov2w1b.txt','w') as f:
#     for i in range(len(cov2w1b)):
#         f. write('%s\n' %cov2w1b)
#
# with open('cov2w1c.txt','w') as f:
#     for i in range(len(cov2w1c)):
#         f. write('%s\n' %cov2w1c)

# # # # regressions of second order phenotype matrices on first order phenotypes.
r2on1a = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
r2on1b = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
r2on1c = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(sites)] for l in range(sites)])
#
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(dm):
                for l in range(dm):
                    for m in range(dm):
                        if var[0][m] > 0.0000000001:
                            r2on1a[i][j][k][l][m] = cov2w1a[i][j][k][l][m]/var[0][m]
                        else:
                            r2on1a[i][j][k][l][m] = 0
                        if varP1i1[1][0][m] > 0.0000000001:
                            r2on1b[i][j][k][l][m] = cov2w1b[i][j][k][l][m]/varP1i1[1][0][m]
                        else:
                            r2on1b[i][j][k][l][m] = 0
                        if varP1D[2][m] > 0.0000000001:
                            r2on1c[i][j][k][l][m] = cov2w1c[i][j][k][l][m]/varP1D[2][m]
                        else:
                            r2on1c[i][j][k][l][m] = 0
#
# with open('r2on1a.txt','w') as f:
#     for i in range(len(r2on1a)):
#         f. write('%s\n' %r2on1a)
# #
# #
# with open('r2on1b.txt','w') as f:
#     for i in range(len(r2on1b)):
#         f. write('%s\n' %r2on1b)
#
#
# with open('r2on1c.txt','w') as f:
#     for i in range(len(r2on1c)):
#         f. write('%s\n' %r2on1c)
#
# cov2w1a = None
# cov2w1b = None
# cov2w1c = None
# #
# # #with printoutput, 35333.59 secs
# # # # Second order polynomials
P2 = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)])
P1Da = array([[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)])
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(N):
                P2[i][j][k] = Q2[i][j][k] - sr.inner_general(r2on1a[i][j],Pa[0][k]) - sr.inner_general(r2on1b[i][j],Pa1i1[1][0][k]) - sr.inner_general(r2on1c[i][j],P1Da[2][k])


r12on1 = r2on1a[0][1]

r12on2i1 = r2on1b[0][1]

PP12 = P2[0][1]
# with open('P2.txt','w') as f:
#     for i in range(len(P2)):
#         f. write('%s\n' %P2)

# # # #print(var2[0][1])
# # #
# # # Second order terms with zeros except for the value that is
# # # present (i.e. orthogonalized within each matrix)
P2a = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)])
for h in range(0,N): # indiv
    for i in range(sites):
        for j in range(sites):
            if j != i:
                P2a[i][j][h] = sr.inner_general(sr.outer_general(phi2[i][j][h],phi2[i][j][h]), P2[i][j][h])
# with open('P2a.txt','w') as f:
#     for i in range(len(P2a)):
#         f. write('%s\n' %P2a)
#
# #with printoutput, ran but took 34683.7 secs (almost 10 hours) and output files are 2 GB long
# #on Quanah, took about 6 hours
# #on Lazarus, took approx 9.5 hours
#
# #PPa12 = P2a[0][1]
# # #
# # # # Covariances between second order phenotypes
cov2w2 = array([[[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(dm)] for l in range(sites)] for m in range(sites)] for n in range(sites)] for p in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    for m in range(sites):
                        if m != l:
                            cov2w2[j][k][l][m] += sr.outer_general(P2[j][k][i], P2[l][m][i])/N
#with open('cov2w2.txt','w') as f:
#     for i in range(len(cov2w2)):
#         f. write('%s\n' %cov2w2)
#
# # Variances of second order phenotypes
var2 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(dm):
                for l in range(dm):
                    var2[i][j][k][l] = cov2w2[i][j][i][j][k][l][k][l]
#
var12 = var2[0][1]
# with open('var2.txt','w') as f:
#     for i in range(len(var2)):
#         f. write('%s\n' %var2)
# #
# # # Variances of second order phenotypes
# for i in range(sites):
#     for j in range(sites):
# 		if j >> i:
# 			for k in range(N):
# 				for l in range(dm):
# 					for m in range(dm):
# 						var2[i][j][l][m] += (P2[i][j][k][l][m]**2)/N - (P2m[i][j][l][m]**2)/N
#
# # # # regressions of second order phenotypes on one another
reg2on2 = array([[[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(dm)] for l in range(sites)] for m in range(sites)] for n in range(sites)] for p in range(sites)])
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(sites):
                for l in range(sites):
                    if l != k:
                        for m in range(dm):
                            for n in range(dm):
                                for o in range(dm):
                                    for p in range(dm):
                                        if var2[k][l][o][p] > 0.0000000001:
                                            reg2on2[i][j][k][l][m][n][o][p] = cov2w2[i][j][k][l][m][n][o][p]/var2[k][l][o][p]
                                        else:
                                            reg2on2[i][j][k][l][m][n][o][p] = 0
# var2 = None
# cov2w2 = None
# with open('reg2on2.txt','w') as f:
#     for i in range(len(reg2on2)):
#         f. write('%s\n' %reg2on2)
# #
# # # # Second order phenotypes independent of one another
P2i2 = array([[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)] for m in range(sites)] for n in range(sites)])
P2i2a = array([[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)] for m in range(sites)] for n in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    for m in range(sites):
                        if m != l:
                            P2i2[j][k][l][m][i] = P2[j][k][i] - sr.inner_general(reg2on2[j][k][l][m],P2a[l][m][i])
                            P2i2a[j][k][l][m][i] = sr.inner_general(sr.outer_general(phi2[j][k][i],phi2[j][k][i]),P2i2[j][k][l][m][i])
# with open('P2i2.txt','w') as f:
#     for i in range(len(P2i2)):
#         f. write('%s\n' %P2i2)
#
# with open('P2i2a.txt','w') as f:
#     for i in range(len(P2i2a)):
#         f. write('%s\n' %P2i2a)
# #
# # # # cov of 2'nd order phi with another independent of the third
cov2w2i2 = array([[[[[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(dm)] for l in range(sites)] for m in range(sites)] for n in range(sites)] for p in range(sites)] for q in range(sites)] for r in range(sites)])
var2i2 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    for m in range(sites):
                        if m != l:
                            for n in range(sites):
                                for o in range(sites):
                                    if o != n:
                                        cov2w2i2[j][k][l][m][n][o] += sr.outer_general(P2[j][k][i],P2i2[l][m][n][o][i])/N
                            for p in range(dm):
                                for q in range(dm):
                                    var2i2[j][k][l][m][p][q] += (P2i2[j][k][l][m][i][p][q]**2)/N
# with open('cov2w2i2.txt','w') as f:
#     for i in range(len(cov2w2i2)):
#         f. write('%s\n' %cov2w2i2)
#
# with open('var2i2.txt','w') as f:
#     for i in range(len(var2i2)):
#         f. write('%s\n' %var2i2)
#
# # # # regressions corresponding to the above
reg2on2i2 = array([[[[[[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)] for k in range(dm)] for l in range(sites)] for m in range(sites)] for n in range(sites)] for p in range(sites)] for q in range(sites)] for r in range(sites)])
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(sites):
                for l in range(sites):
                    if l != k:
                        for k1 in range(sites):
                            for l1 in range(sites):
                                if l1 != k1:
                                    for m in range(dm):
                                        for n in range(dm):
                                            for o in range(dm):
                                                for p in range(dm):
                                                    if var2i2[k][l][k1][l1][o][p] > 0.0000000001:
                                                        reg2on2i2[i][j][k][l][k1][l1][m][n][o][p] = cov2w2i2[i][j][k][l][k1][l1][m][n][o][p]/var2i2[k][l][k1][l1][o][p]
                                                    else:
                                                        reg2on2i2[i][j][k][l][k1][l1][m][n][o][p] = 0
# cov2w2i2 = None
# var2i2 = None
#
# with open('reg2on2i2.txt','w') as f:
#     for i in range(len(reg2on2i2)):
#         f. write('%s\n' %reg2on2i2)
#
# #
# #
# # # # 2'nd order phi independent of all others
P2D = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)])
P2Da = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(N)] for k in range(sites)] for l in range(sites)])
for i in range(1):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    for m in range(sites):
                        if m != l and (l,m) != (j,k) and (l,m) != (k,j):
                            for n in range(sites):
                                for o in range(sites):
                                    if o != n and (n,o) != (l,m) and (n,o) != (m,l) and (n,o) != (j,k) and (n,o) != (k,j):
                                        P2D[j][k][i] = P2[j][k][i] - sr.inner_general(reg2on2i2[j][k][l][m][n][o],P2i2a[l][m][n][o][i]) - sr.inner_general(reg2on2[j][k][n][o],P2a[n][o][i])
                                        P2Da[j][k][i] = sr.inner_general(sr.outer_general(phi2[j][k][i],phi2[j][k][i]),P2D[j][k][i])
# reg2on2 = None
# reg2on2i2 = None
# with open('P2D.txt','w') as f:
#     for i in range(len(P2D)):
#         f. write('%s\n' %P2D)
#
# with open('P2Da.txt','w') as f:
#     for i in range(len(P2Da)):
#         f. write('%s\n' %P2Da)
# P2Da = None
#
# #
# # #
# # # #variance in P2D
var2D = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(dm):
                    for m in range(dm):
                        var2D[j][k][l][m] += (P2D[j][k][i][l][m]**2)/N

# ------------Projecting the trait values into the space of orthogonal ----
# -------------polynomials --------------------------------------------------------------------

# initializing arrays

cov1FP = array([[0.0 for z in range(dm)] for i in range(sites)])
covFP = array([[0.0 for z in range(dm)] for i in range(sites)])
covFPP = array([[0.0 for z in range(dm)] for i in range(dm)])
rFon1 = array([0.0 for z in range(dm)])
rFon2 = array([0.0 for z in range(dm)])
rFon2i1 = array([0.0 for z in range(dm)])
rFon12 = array([[0.0 for z in range(dm)] for i in range(dm)])

covFw1 = array([[0.0 for z in range(dm)] for i in range(sites)])
covFw1i1 = array([[[0.0 for z in range(dm)] for i in range(sites)] for j in range(sites)])
covFw1D = array([[0.0 for z in range(dm)] for i in range(sites)])

rFon1 = array([[0.0 for z in range(dm)] for i in range(sites)])
rFon1i1 = array([[[0.0 for z in range(dm)] for i in range(sites)] for j in range(sites)])
rFon1D = array([[0.0 for z in range(dm)] for i in range(sites)])

covFw2 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
covFw2i2 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
covFw2D = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])

rFon2 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
rFon2i2 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
rFon2D = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])

covFw3 = array([[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)])
rFon3 = array([[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)])

# Calculating the mean trait value
Fm = 0
for i in range(0, N):  # individuals
    Fm += F[i] / N

# Covariances of the trait with each element of the 1'st order
# vectors.
# We can use the 'dot' operator here to get the inner product of
# a first and a second rank tensor (a vector and a matrix).
covFP[0] = dot(F, P[0]) / N  # for site 1
cov1FP[1] = dot(F, P[1]) / N
covFP[1] = dot(F, P2i1) / N  # for site 2 independent of 1
print(covFP[0])
print(cov1FP[0])
print(covFP[1])

for i in range(N):
    for j in range(sites):
        for k in range(dm):
            covFw1[j][k] += F[i] * P[j][i][k] / N
            covFw1D[j][k] += F[i] * P1D[j][i][k] / N
        for l in range(sites):
            if l != j:
                for m in range(dm):
                    covFw1i1[j][l][m] += F[i] * P1i1[j][l][i][m] / N

for i in range(N):
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(dm):
                    for m in range(dm):
                        covFw2[j][k][l][m] += F[i]*P2[j][k][i][l][m]/N
                        covFw2D[j][k][l][m] += F[i]*P2D[j][k][i][l][m]/N
                for n in range(sites):
                    for o in range(sites):
                        if n!= o:
                            for p in range(dm):
                                for q in range(dm):
                                    covFw2i2[j][k][n][o][p][q] += F[i]*P2i2[j][k][n][o][i][p][q]/N

# for i in range(N):
#     for j in range(dm):
#         for k in range(dm):
#             for l in range(dm):
#                 covFw3[j][k][l] += F[i]*P3[i][j][k][l]/N


# Covariance of the trait with each element of the second order
# phenotype.
# Note: We can NOT use the 'dot' operator here because we do not
# have a matrix times a vector.
# for i in range(0,N):  #indiv
#     covFPP += (F[i]*PP12[i]/N)

# print("covFPP ="+str(covFPP))
print("working on rFon1 and rFon1D")
# Regressions of the trait on each element of the first order
# phenotype vectors.
for j in range(sites):
    for i in range(0, dm):  # nucleotides
        if var[j][i] > 0.0000000001:
            rFon1[j][i] = covFw1[j][i] / var[j][i]
        else:
            rFon1[j][i] = 0
        if varP1D[j][i] > 0.0000000001:
            rFon1D[j][i] = covFw1D[j][i] / varP1D[j][i]
        else:
            rFon1D[j][i] = 0
print('saving rFon1 to: rFon1_s1.npy')
np.save("rFon1_s1.npy", rFon1)

print('saving rFon1D to: rFon1D_s1.npy')
np.save("rFon1D_s1.npy", rFon1D)
# with open('rFon1.txt', 'w') as f:
#     for i in range(len(rFon1)):
#         f.write('%s\n' % rFon1)

for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(dm):
                if varP1i1[i][j][k] > 0.00000000001:
                    rFon1i1[i][j][k] = covFw1i1[i][j][k] / varP1i1[i][j][k]
                else:
                    rFon1i1[i][j][k] = 0

# Regressions of the trait on each element of the second order
# phenotype matrices.

for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(dm):
                for l in range(dm):
                    if var2[i][j][k][l] > 0.00000000001:
                        rFon2[i][j][k][l] = covFw2[i][j][k][l]/var2[i][j][k][l]
                    else:
                        rFon2[i][j][k][l] = 0
                    if var2D[i][j][k][l] > 0.00000000001:
                        rFon2D[i][j][k][l] = covFw2D[i][j][k][l]/var2D[i][j][k][l]
                    else:
                        rFon2D[i][j][k][l] = 0
#
for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(sites):
                for l in range(sites):
                    if l != k:
                        for m in range(dm):
                            for n in range(dm):
                                if var2i2[i][j][k][l][m][n] > 0.0000000001:
                                    rFon2i2[i][j][k][l][m][n] = covFw2i2[i][j][k][l][m][n]/var2i2[i][j][k][l][m][n]
                                else:
                                    rFon2i2[i][j][k][l][m][n] = 0

# for i in range(dm):
#     for j in range(dm):
#         for k in range(dm):
#             if var3[i][j][k] > 0.0000000001:
#                 rFon3[i][j][k] = covFw3[i][j][k]/var3[i][j][k]
#             else:
#                 rFon3[i][j][k] = 0
#
print("rFon2i1"+str(rFon2i1))
#
# # Regressions of the trait on each element of the second order
# # phenotype matrix.
for i in range(0,dm): #nucleotide 1
    for j in range(0,dm):  #nucleotide 2
        if var12[i][j] > 0.0000000001:
            rFon12[i][j] = covFPP[i][j]/var12[i][j]
        else:
            rFon12[i] = 0

print("rFon12"+str(rFon12))

# Contribution of site 1 for each individual.
# This is the regression of the trait on site 1 times (inner product)
# the individual's site 1 vector that has been orthogonalized within
# the vector.
for i in range(0, N):
    # test_val = sr.inner_general(rFon1[0],Pa[0][i])
    Fon1[i] = sr.inner_general(rFon1[0], Pa[0][i])

# Contribution of site 2 independent of 1 for each individual.
for i in range(0, N):
    Fon2i1[i] = sr.inner_general(rFon1i1[1][0], Pa1i1[1][0][i])
#
# # Contribution of the second order phenotype for each individual.
for i in range(0,N):
    Fon12[i] = sr.inner_general(rFon2[0][1],P2a[0][1][i])

# Ignoring very small values that would be due to roundoff error.
# Change or delete this for a large data set.
for i in range(0, N):
    if fabs(Fon1[i]) < 0.0000000000001:
        Fon1[i] = 0
    if fabs(Fon2i1[i]) < 0.0000000000001:
        Fon2i1[i] = 0
    if fabs(Fon12[i]) < 0.0000000000001:
        Fon12[i] = 0

# -----------------------Listing the main results------------------

print('Regression of trait on site 1')
print(rFon1)
print
print('Regression of trait on site 2')
print(rFon2)
print
print('Regression of trait on site 2 independent of 1')
print(rFon2i1)
print
print('Regression on (site 1)x(site 2), independent of first order')
print(rFon12)

end = time.time()
print(end - start_time)

