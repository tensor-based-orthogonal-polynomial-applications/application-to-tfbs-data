from tfbs_twositecombinations_regressions import orthogonal_polynomial
import numpy as np
import glob
import os

folder = '/home/snafees/TFBS_work/cbf1/cbf1_nn/second_order/'

csvs = glob.glob(os.path.join(folder, "*.csv"))

for filename in csvs:
    orthogonal_polynomial(filename, '/home/snafees/TFBS_work/cbf1/cbf1_nn/cbf1_nn_pred_s1_ddg.csv', sites=2, dm=4, N=10000)
