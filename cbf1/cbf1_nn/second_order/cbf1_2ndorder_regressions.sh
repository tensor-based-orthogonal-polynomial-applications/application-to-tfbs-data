#!/bin/sh
#$ -V
#$ -cwd
#$ -S /bin/bash
#$ -N cbf1_2ndorder_regressions
#$ -o $JOB_NAME.o$JOB_ID
#$ -e $JOB_NAME.e$JOB_ID
#$ -q omni
#$ -pe mpi 36
#$ -P quanah

module load intel/18.0.3.222
module load python3

from tfbs_twositecombinations_regressions import orthogonal_polynomial
import numpy as np
import glob
import os

python3 for file in glob.glob(os.path.join(/home/snafees/TFBS_work/cbf1/cbf1_nn/second_order/, "*.csv")):
  orthogonal_polynomial(file, '/home/snafees/TFBS_work/cbf1/cbf1_nn/cbf1_nn_pred_s1_ddg.csv', sites=2, dm=4, N=10000)
