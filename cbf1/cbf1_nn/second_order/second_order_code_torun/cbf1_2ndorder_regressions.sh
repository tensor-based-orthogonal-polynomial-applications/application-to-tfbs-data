#!/bin/sh
#$ -V
#$ -cwd
#$ -S /bin/bash
#$ -N cbf1_2ndorder_regressions
#$ -o $JOB_NAME.o$JOB_ID
#$ -e $JOB_NAME.e$JOB_ID
#$ -q omni
#$ -pe mpi 36
#$ -P quanah

module load intel/18.0.3.222
module load python3

python3 cbf1_2ndorder_pythonprogramtorunallfiles.py

