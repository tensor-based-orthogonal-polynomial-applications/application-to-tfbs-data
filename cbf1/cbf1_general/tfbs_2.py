from numpy import *
import numpy as np
import matplotlib.pyplot as plt
import sr
import sys
import time
start_time = time.time()

#N = 1008915 # Population size (number of samples)
N = 851027 #after getting rid of the -inf values for ddg
dm = 4  # Dimension
sites = 10  # Number of sites
# ------- Getting trait values from file 'trait.dat'-----------
F = genfromtxt('ddg_cleaned_1.txt')  # file containing trait values that will be mapped to sequence
Fest = genfromtxt('ddg_cleaned_1.txt') # vectors that must be the same size as F
Fon1 = genfromtxt('ddg_cleaned_1.txt')
Fon2i1 = genfromtxt('ddg_cleaned_1.txt')
Fon12 = genfromtxt('ddg_cleaned_1.txt')

for i in range(N):
   Fest[i] = 0
   Fon1[i] = 0
   Fon2i1[i] = 0
   Fon12[i] = 0

file = open('cbf1_cleaned_seqs.txt')
seq = file.readlines()

# ----Initializing various terms that we will use.--------------

# 3 sites, each a dm dim vector, in N individuals
# NOTE: For application to Amino Acid sequences, increase
# the size of the arrays accordingly.
phi = array([[[0.0 for k in range(dm)] for i in range(N)] for j in range(sites)])  # general enough for all sites
mean = array([[0.0 for z in range(dm)] for i in range(sites)])
var = array([[0.0 for z in range(dm)] for i in range(sites)])
phi2 = array([[[[[0.0 for k in range(dm)] for i in range(dm)] for j in range(N)] for l in range(sites)] for m in range(sites)])
phi2m = array([[[[0.0 for k in range(dm)] for i in range(dm)] for l in range(sites)] for m in range(sites)])

phi3 = array([[[[0.0 for k in range(dm)] for i in range(dm)] for j in range(dm)] for l in range(N)])
phi3m = array([[[0.0 for k in range(dm)] for i in range(dm)] for j in range(dm)])

P = array([[[0.0 for z in range(dm)] for j in range(N)] for i in range(sites)])
#CM = array([[[0.0 for k in range(dm)] for i in range(sites)] for m in range(0, M)])
cov = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])

# # ------------Converting letters to vectors---------------
#
count = array([0.0 for j in range(sites)])
# #phi[individual][site][state]. phi[i][j] = vector for site j in individual i.
for i in range(0, N):  # individual
    for j in range(sites):
        if seq[i][j] == 'A':
            phi[j][i][0] = 1.0
            count[j] += 1
        if seq[i][j] == 'a':
            phi[j][i][0] = 1.0
            count[j] += 1
        if seq[i][j] == 'T':
            phi[j][i][1] = 1.0
            count[j] += 1
        if seq[i][j] == 't':
            phi[j][i][1] = 1.0
            count[j] += 1
        if seq[i][j] == 'G':
            phi[j][i][2] = 1.0
            count[j] += 1
        if seq[i][j] == 'g':
            phi[j][i][2] = 1.0
            count[j] += 1
        if seq[i][j] == 'C':
            phi[j][i][3] = 1.0
            count[j] += 1
        if seq[i][j] == 'c':
            phi[j][i][3] = 1.0
            count[j] += 1
        if phi[j][i][0] == 0.0:
            if phi[j][i][1] == 0.0:
                if phi[j][i][2] == 0.0:
                    if phi[j][i][3] == 0.0:
                        phi[j][i][0] = -1

# keep in alpha order

# ---------------------------------First order terms --------------------------------
np.set_printoptions(threshold=sys.maxsize)

mean = np.load("mean.npy")

P = np.load("P.npy")
# #var[site][nucleotide]

var = np.load("var.npy")

cov = np.load("covariance2.npy") #the first cov was done with the seqs not filtered according to -inf ddg values

Pa = array([[[0.0 for z in range(dm)] for j in range(N)] for i in range(sites)])
reg11 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])


reg11 = np.load("reg11.npy")

PakiDim = np.load("PakiDim.npy")

P1i1 = np.load("P1i1.npy")

P2i1 = P1i1[1][0]

varP1i1 = np.load("varP1i1.npy")

print("working on cov11i1")
# # # # cov11i1[j][k][l] = cov between site j and (site k independent of l)
cov11i1 = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)])

for j in range(sites):
    for k in range(sites):
        for l in range(sites):
            for i in range(N):
                cov11i1[j][k][l] += sr.outer_general(P[j][i],P1i1[k][l][i])/N

print('saving cov11i1 to: cov11i1.npy')
np.save("cov11i1.npy", cov11i1)

print("working on reg11i1")
# # # # # regression of site j on (site k independent of l)
reg11i1 = array([[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)])

for k in range(sites):
    for l in range(sites):
        for m in range(sites):
            for i in range(0,dm):
                for j in range(0,dm):
                    if varP1i1[l][m][j] > 0.0000000000001:
                        reg11i1[k][l][m][i][j] = cov11i1[k][l][m][i][j]/varP1i1[l][m][j]
                    else:
                        reg11i1[k][l][m][i][j] = 0
print('saving reg11i1 to: reg11i1.npy')
np.save("reg11i1.npy", reg11i1)

# #dump cov11i1 = None
cov11i1 = None
#
# # # #ran but took 5903.9 secs
# # #with printoutput, took 38695.98 secs (but this may also be due to computer dying or sleep at night
print("working on Pa1i1")
# # # # # same as P1i1, except with all elements = 0 except the one present.
Pa1i1 = array([[[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)] for k in range(sites)])

for i in range(0,N):  # indiv
    for j in range(sites):
        for k in range(sites):
            if k != j:
                Pa1i1[j][k][i] = sr.inner_general(sr.outer_general(phi[j][i],phi[j][i]),P1i1[j][k][i])

print('saving Pa1i1 to: Pa1i1.npy')
np.save("Pa1i1.npy", Pa1i1)
# #
print("working on P1D")
# # # # # P1D[j][i] = first order poly of site j independent of all other sites, for individual i
P1D = array([[[0.0 for z in range(dm)] for i in range(N)] for j in range(sites)])
#
for i in range(0,N):  # indiv
    for j in range(sites):
        for k in range(sites):
            if k != j:
                for l in range(sites):
                    if l != k & l != j:
                        P1D[j][i] = P[j][i] - sr.inner_general(reg11i1[j][k][l],Pa1i1[k][l][i]) - sr.inner_general(reg11[j][l],Pa[l][i])

print('saving P1D to: P1D.npy')
np.save("P1D.npy", P1D)

reg11i1 = None

# # # #  P1D with all elements = 0 except the one present
print("working on varP1D")
# # # #variance in P1D
varP1D = array([[0.0 for z in range(dm)] for i in range(sites)])

for k in range(sites):
    for i in range(0,dm):  # nucleotide
        for j in range(0,N):   # individual
            varP1D[k][i] += ((P1D[k][j][i])**2)/N

print('saving varP1D to: varP1D.npy')
np.save("varP1D.npy", varP1D)
# with open('varP1D.txt','w') as f:
#     for i in range(len(varP1D)):
#         f. write('%s\n' %varP1D)
Pa2i1 = Pa1i1[1][0]
varP2i1 = varP1i1[1][0]

# ------------Projecting the trait values into the space of orthogonal ----
# -------------polynomials --------------------------------------------------------------------

# initializing arrays

cov1FP = array([[0.0 for z in range(dm)] for i in range(sites)])
covFP = array([[0.0 for z in range(dm)] for i in range(sites)])
covFPP = array([[0.0 for z in range(dm)] for i in range(dm)])
rFon1 = array([0.0 for z in range(dm)])
rFon2 = array([0.0 for z in range(dm)])
rFon2i1 = array([0.0 for z in range(dm)])
rFon12 = array([[0.0 for z in range(dm)] for i in range(dm)])

covFw1 = array([[0.0 for z in range(dm)] for i in range(sites)])
covFw1i1 = array([[[0.0 for z in range(dm)] for i in range(sites)] for j in range(sites)])
covFw1D = array([[0.0 for z in range(dm)] for i in range(sites)])

rFon1 = array([[0.0 for z in range(dm)] for i in range(sites)])
rFon1i1 = array([[[0.0 for z in range(dm)] for i in range(sites)] for j in range(sites)])
rFon1D = array([[0.0 for z in range(dm)] for i in range(sites)])

# covFw2 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
# covFw2i2 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
# covFw2D = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
#
# rFon2 = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
# rFon2i2 = array([[[[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)] for l in range(sites)] for m in range(sites)])
# rFon2D = array([[[[0.0 for z in range(dm)] for i in range(dm)] for j in range(sites)] for k in range(sites)])
#
# covFw3 = array([[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)])
# rFon3 = array([[[0.0 for z in range(dm)] for i in range(dm)] for j in range(dm)])

# Calculating the mean trait value
Fm = 0
for i in range(0, N):  # individuals
    Fm += F[i] / N

# Covariances of the trait with each element of the 1'st order
# vectors.
# We can use the 'dot' operator here to get the inner product of
# a first and a second rank tensor (a vector and a matrix).
covFP[0] = dot(F, P[0]) / N  # for site 1
cov1FP[1] = dot(F, P[1]) / N
covFP[1] = dot(F, P2i1) / N  # for site 2 independent of 1
print(covFP[0])
print(cov1FP[0])
print(covFP[1])

for i in range(N):
    for j in range(sites):
        for k in range(dm):
            covFw1[j][k] += F[i] * P[j][i][k] / N
            covFw1D[j][k] += F[i] * P1D[j][i][k] / N
        for l in range(sites):
            if l != j:
                for m in range(dm):
                    covFw1i1[j][l][m] += F[i] * P1i1[j][l][i][m] / N

# for i in range(N):
#  for j in range(sites):
#   for k in range(sites):
#    if k != j:
#     for l in range(dm):
#      for m in range(dm):
#       covFw2[j][k][l][m] += F[i]*P2[j][k][i][l][m]/N
#       covFw2D[j][k][l][m] += F[i]*P2D[j][k][i][l][m]/N
#     for n in range(sites):
#      for o in range(sites):
#       if n!= o:
#        for p in range(dm):
#         for q in range(dm):
#          covFw2i2[j][k][n][o][p][q] += F[i]*P2i2[j][k][n][o][i][p][q]/N
#
# for i in range(N):
#     for j in range(dm):
#         for k in range(dm):
#             for l in range(dm):
#                 covFw3[j][k][l] += F[i]*P3[i][j][k][l]/N


# Covariance of the trait with each element of the second order
# phenotype.
# Note: We can NOT use the 'dot' operator here because we do not
# have a matrix times a vector.
# for i in range(0,N):  #indiv
#     covFPP += (F[i]*PP12[i]/N)

# print("covFPP ="+str(covFPP))
print("working on rFon1 and rFon1D")
# Regressions of the trait on each element of the first order
# phenotype vectors.
for j in range(sites):
    for i in range(0, dm):  # nucleotides
        if var[j][i] > 0.0000000001:
            rFon1[j][i] = covFw1[j][i] / var[j][i]
        else:
            rFon1[j][i] = 0
        if varP1D[j][i] > 0.0000000001:
            rFon1D[j][i] = covFw1D[j][i] / varP1D[j][i]
        else:
            rFon1D[j][i] = 0
print('saving rFon1 to: rFon1.npy')
np.save("rFon1.npy", rFon1)

print('saving rFon1D to: rFon1D.npy')
np.save("rFon1D.npy", rFon1D)
# with open('rFon1.txt', 'w') as f:
#     for i in range(len(rFon1)):
#         f.write('%s\n' % rFon1)

for i in range(sites):
    for j in range(sites):
        if j != i:
            for k in range(dm):
                if varP1i1[i][j][k] > 0.00000000001:
                    rFon1i1[i][j][k] = covFw1i1[i][j][k] / varP1i1[i][j][k]
                else:
                    rFon1i1[i][j][k] = 0

# Regressions of the trait on each element of the second order
# phenotype matrices.

# for i in range(sites):
#     for j in range(sites):
#         if j != i:
#             for k in range(dm):
#                 for l in range(dm):
#                     if var2[i][j][k][l] > 0.00000000001:
#                         rFon2[i][j][k][l] = covFw2[i][j][k][l]/var2[i][j][k][l]
#                     else:
#                         rFon2[i][j][k][l] = 0
#                     if var2D[i][j][k][l] > 0.00000000001:
#                         rFon2D[i][j][k][l] = covFw2D[i][j][k][l]/var2D[i][j][k][l]
#                     else:
#                         rFon2D[i][j][k][l] = 0
#
# for i in range(sites):
#  for j in range(sites):
#   if j != i:
#    for k in range(sites):
#     for l in range(sites):
#      if l != k:
#       for m in range(dm):
#        for n in range(dm):
#         if var2i2[i][j][k][l][m][n] > 0.0000000001:
#          rFon2i2[i][j][k][l][m][n] = covFw2i2[i][j][k][l][m][n]/var2i2[i][j][k][l][m][n]
#         else:
#          rFon2i2[i][j][k][l][m][n] = 0
#
# for i in range(dm):
#     for j in range(dm):
#         for k in range(dm):
#             if var3[i][j][k] > 0.0000000001:
#                 rFon3[i][j][k] = covFw3[i][j][k]/var3[i][j][k]
#             else:
#                 rFon3[i][j][k] = 0
#
# print("rFon2i1"+str(rFon2i1))
#
# # Regressions of the trait on each element of the second order
# # phenotype matrix.
# for i in range(0,dm): #nucleotide 1
#     for j in range(0,dm):  #nucleotide 2
#         if var12[i][j] > 0.0000000001:
#             rFon12[i][j] = covFPP[i][j]/var12[i][j]
#         else:
#             rFon12[i] = 0
#
# print("rFon12"+str(rFon12))

# Contribution of site 1 for each individual.
# This is the regression of the trait on site 1 times (inner product)
# the individual's site 1 vector that has been orthogonalized within
# the vector.
for i in range(0, N):
    # test_val = sr.inner_general(rFon1[0],Pa[0][i])
    Fon1[i] = sr.inner_general(rFon1[0], Pa[0][i])

# Contribution of site 2 independent of 1 for each individual.
for i in range(0, N):
    Fon2i1[i] = sr.inner_general(rFon1i1[1][0], Pa1i1[1][0][i])
#
# # Contribution of the second order phenotype for each individual.
# for i in range(0,N):
#     Fon12[i] = sr.inner_general(rFon2[0][1],P2a[0][1][i])

# Ignoring very small values that would be due to roundoff error.
# Change or delete this for a large data set.
for i in range(0, N):
    if fabs(Fon1[i]) < 0.0000000000001:
        Fon1[i] = 0
    if fabs(Fon2i1[i]) < 0.0000000000001:
        Fon2i1[i] = 0
    if fabs(Fon12[i]) < 0.0000000000001:
        Fon12[i] = 0

# -----------------------Listing the main results------------------

print('Regression of trait on site 1')
print(rFon1)
print
print('Regression of trait on site 2')
print(rFon2)
print
print('Regression of trait on site 2 independent of 1')
print(rFon2i1)
print
print('Regression on (site 1)x(site 2), independent of first order')
print(rFon12)

end = time.time()
print(end - start_time)

